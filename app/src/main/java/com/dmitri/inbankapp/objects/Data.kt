package com.dmitri.inbankapp.objects


class Data {
    companion object {
        const val DEBT = -1
        const val SEGMENT_1 = 100
        const val SEGMENT_2 = 300
        const val SEGMENT_3 = 1000
    }

    val candidates: Map<String, Int> = mapOf(
        "49002010965" to DEBT,
        "49002010976" to SEGMENT_1,
        "49002010987" to SEGMENT_2,
        "49002010998" to SEGMENT_3
    )


}