package com.dmitri.inbankapp.activities

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import android.widget.SeekBar
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.dmitri.inbankapp.objects.Data
import com.dmitri.inbankapp.viewmodels.MainViewModel
import com.dmitri.inbankapp.R
import com.dmitri.inbankapp.utils.LoanValidator
import com.dmitri.inbankapp.objects.Loan
import com.dmitri.inbankapp.utils.checkIdCode
import com.dmitri.inbankapp.utils.getResultText

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val viewModel: MainViewModel by lazy {
            ViewModelProvider(this).get(MainViewModel::class.java)
        }
        runAmountSetup(viewModel)
        runDurationSetup(viewModel)
        runResultButtonSetup(viewModel)
    }

    private fun runAmountSetup(
            viewModel: MainViewModel
    ) {
        val loanAmountTextView = findViewById<TextView>(R.id.tv_loan_amount)
        val loanAmountSeekBar = findViewById<SeekBar>(R.id.sb_loan_amount)
        loanAmountSeekBar.max = Loan.MAX_AMOUNT / 100 - 20

        loanAmountTextView.text =
                String.format(getString(R.string.eur_amount, viewModel.loan.amount))
        loanAmountSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                viewModel.loan.amount = (progress + 20) * 100
                loanAmountTextView.text =
                        String.format(getString(R.string.eur_amount, viewModel.loan.amount))
                clearResultText()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })
    }


    private fun runDurationSetup(
            viewModel: MainViewModel
    ) {
        val loanDurationTextView = findViewById<TextView>(R.id.tv_loan_duration)
        val loanDurationSeekBar = findViewById<SeekBar>(R.id.sb_loan_duration)
        loanDurationSeekBar.max = Loan.MAX_DURATION - 12
        loanDurationTextView.text = String.format(
                getString(R.string.month_num, viewModel.loan.duration)
        )
        loanDurationSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                viewModel.loan.duration = progress + 12
                loanDurationTextView.text = String.format(
                        getString(R.string.month_num, viewModel.loan.duration)
                )
                clearResultText()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }

        })
    }

    private fun runResultButtonSetup(viewModel: MainViewModel) {
        val resultButton = findViewById<Button>(R.id.button_result)
        val personalIdEditText = findViewById<EditText>(R.id.et_personal_code)
        val resultTextView = findViewById<TextView>(R.id.tv_result)
        val maxLoanTextView = findViewById<TextView>(R.id.tv_max_amount)

        var id = personalIdEditText.text.toString() // 49002010976
        personalIdEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                id = s.toString()
                clearResultText()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        resultButton.setOnClickListener { _ ->
            run {
                clearResultText()
                val maxAmount = Data().candidates.get(id)?.let {
                    LoanValidator(
                            viewModel.loan,
                            it
                    ).maxLoan()
                }

                if (id == "") {
                    personalIdEditText.error = getString(R.string.error_missing_id)
                } else if (!checkIdCode(id)) {
                    personalIdEditText.error = getString(R.string.error_invalid_id)
                } else if (!Data().candidates.containsKey(id)) {
                    resultTextView.text =
                            getString(R.string.result_no_data)
                } else {
                    if (maxAmount != null) {
                        resultTextView.text = getResultText(this, id, viewModel.loan, maxAmount)
                        if (maxAmount > 0) {
                            maxLoanTextView.text =
                                    String.format(
                                            getString(R.string.eur_amount),
                                            maxAmount
                                    )
                        }
                    }
                }
            }


        }
    }


    private fun clearResultText() {
        findViewById<TextView>(R.id.tv_result).text = ""
        findViewById<TextView>(R.id.tv_max_amount).text = ""
    }


}
