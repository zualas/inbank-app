package com.dmitri.inbankapp.utils

import com.dmitri.inbankapp.objects.Data
import com.dmitri.inbankapp.objects.Loan

class LoanValidator(private val loan: Loan, private val modifier: Int) {
    companion object {
        const val LOAN_TOO_BIG = -1
        const val LOAN_REJECTED = -2
    }

    fun maxLoan(): Int {
        if (this.modifier == Data.DEBT) return LOAN_REJECTED
        val result = modifier / loan.amount * loan.duration
        val max = (modifier * loan.duration) / 100 * 100
        return if (result >= 1) loan.amount
        else return if (max < Loan.MIN_AMOUNT) LOAN_TOO_BIG
        else return if (max > Loan.MAX_AMOUNT) Loan.MAX_AMOUNT else max
    }

}