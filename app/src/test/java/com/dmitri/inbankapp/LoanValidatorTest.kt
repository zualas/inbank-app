package com.dmitri.inbankapp

import com.dmitri.inbankapp.utils.LoanValidator
import com.dmitri.inbankapp.objects.Data
import com.dmitri.inbankapp.objects.Loan
import org.junit.Assert
import org.junit.Test

class LoanValidatorTest {

    @Test
    fun loanValidator_pass_segment_2() {
        val validator = LoanValidator(Loan(3000, 12), Data.SEGMENT_2)
        Assert.assertTrue(validator.maxLoan() == 3600)
    }

    @Test
    fun loanValidator_pass_segment_1() {
        val validator = LoanValidator(Loan(3000, 24), Data.SEGMENT_1)
        Assert.assertTrue(validator.maxLoan() == 2400)
    }

    @Test
    fun loanValidator_fail_debt() {
        val validator = LoanValidator(Loan(2000, 12), Data.DEBT)
        Assert.assertTrue(validator.maxLoan() == LoanValidator.LOAN_REJECTED)
    }

    @Test
    fun loanValidator_exception() {
        try {
            val validator = LoanValidator(Loan(50, 12), Data.DEBT)
        }
        catch (e:UnsupportedOperationException) {
            Assert.assertTrue(e.message == "Arguments out of limits!")
        }
    }
}