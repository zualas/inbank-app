<h1>Home task</h1>

Build instructions:
1. Open in Android Studio
2. Enable USB debugging on your Android device
3. Connect your Android Device via USB
4. Press "Run" button in Android Studio
0. Alternatively, this can all be done in the emulator

What the project consists of:
- Single screen with control elements
- Code written in Kotlin
- Supports all Android devices on API
- English interface with a possibility to implement additional languages easily

What works:
- Requested functionality as closely as I could understand it
- Basic unit tests for loan validation
- Portrait and landscape mode
- ViewModel to preserve loan data in case of Activity destruction
- A few custom drawable elements to make the UI look nicer

What was skipped due to the very limited scope:
- More clear entity division. In a bigger project, a Profile object with ID code and loan as dependency would be appropriate. With the current scope, it is not necessary.
- Animations. They wouldn't add much to the UX atm, but take significant time to implement
- Data binding in XML. This is a great feature in bigger interfaces, but takes excessive time to setup
- Android instrumentation tests. The app is small enough to test it manually

What could be done next if this was a real customer request:
- Elements in Fragments instead of just layouts. This would give more flexibility to adapt to various screen
- Design system and UI improvements
- Preservation of validation result over screen rotation
