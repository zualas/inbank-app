package com.dmitri.inbankapp.utils

import android.content.Context
import androidx.core.text.trimmedLength
import com.dmitri.inbankapp.R
import com.dmitri.inbankapp.objects.Loan
import com.dmitri.inbankapp.viewmodels.MainViewModel

/**
 * Absolutely basic Estonian Personal ID check
 */
fun checkIdCode(idCode: String): Boolean {
    return idCode.trimmedLength() == 11
}

fun getResultText(context: Context, id: String, loan: Loan, maxAmount: Int): String {

    var approveText: String = ""

    approveText =
        if (maxAmount >= loan.amount) context.getString(R.string.approve) else context.getString(
            R.string.reject
        )

    if (maxAmount == LoanValidator.LOAN_REJECTED) {
        return context.getString(R.string.loan_rejected)

    } else if (maxAmount == LoanValidator.LOAN_TOO_BIG) {
        return context.getString(R.string.loan_too_big)
    } else {
        return String.format(
            context.getString(R.string.loan_approved), approveText,
            loan.duration
        )

    }

}