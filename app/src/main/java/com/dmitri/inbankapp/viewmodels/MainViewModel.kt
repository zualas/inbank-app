package com.dmitri.inbankapp.viewmodels

import android.widget.EditText
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.InverseMethod
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dmitri.inbankapp.objects.Loan

class MainViewModel() : ViewModel() {
   var loan: Loan = Loan(Loan.MIN_AMOUNT, Loan.MIN_DURATION)
}
