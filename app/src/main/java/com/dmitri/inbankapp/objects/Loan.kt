package com.dmitri.inbankapp.objects

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import java.lang.UnsupportedOperationException

data class Loan(var amount: Int, var duration: Int) {
    companion object {
        const val MIN_AMOUNT = 2000
        const val MIN_DURATION = 12 // months
        const val MAX_AMOUNT = 10000
        const val MAX_DURATION = 60 // months
    }

    init {
        if (amount < MIN_AMOUNT || amount > MAX_AMOUNT || duration < MIN_DURATION || duration > MAX_DURATION) {
            throw UnsupportedOperationException("Arguments out of limits!")
        }
    }

}
